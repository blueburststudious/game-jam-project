using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public Movement Player;

    void OnTriggerEnter2D(Collider2D Box)
    {
        switch (Box.gameObject.layer)
        {
            case 10:
                // player
                break;
            case 6:
           
                // Power Box (Powers a certain item such as a jump pad or a gate)
                int children = Box.gameObject.GetComponentsInChildren<Transform>().Length;

                for (int i = 0; i <= children; i++)
                {
                    Transform item = Box.gameObject.transform.GetChild(i);
                }

                Destroy(gameObject);
                break;
			

            case 7:
                
                // Flip Box (Flips player around when hit)
                Player.FlipPlayer();
                Destroy(gameObject);
                break;
             
            case 8:
                
                // Mirror
               MirrorAttributes att = Box.gameObject.GetComponent<MirrorAttributes>() as MirrorAttributes;
               Vector2[] tab = att.directions;
               Rigidbody2D rigid = gameObject.GetComponent<Rigidbody2D>();

               gameObject.transform.eulerAngles = new Vector3(0f, 0f, gameObject.transform.rotation.z + 90f);
               if (Mathf.Abs(gameObject.transform.rotation.z / 90f) == 1f) {
                    rigid.velocity = tab[0] * 10f;
			   } else {
                    rigid.velocity = tab[1] * 10f;     
			   }
               break;
            

		}
    }
}
